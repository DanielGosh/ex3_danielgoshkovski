#include "Vector.h"
/*
construction function for the vector
*/
Vector::Vector(int n)
{
	int val = n;
	if (val < 2)
	{
		val = 2;
	}
	this->_size = val;
	this->_elements = new int[val];
	this->_resizeFactor = this->_size;
	this->_capacity = this->_size * 2;

}
/*
deconstructor for the vector
*/
Vector::~Vector()
{
	delete[] this->_elements;
	this->_size = 0;
}
/*
getter for the size value
*/
int Vector::size()
{
	return this->_size;
}
/*
getter for the capacity value
*/
int Vector::capicity()
{
	return this->_capacity;
}
/*
getter for the resizeFactor value
*/
int Vector::resizeFactor()
{
	return this->_resizeFactor;
}
/*
function to check if the elements list is empty
*/
bool Vector:: empty()
{
	if (this->_elements)
	{
		return false;
	}
	return true;
}
/*
function to push back a value to the end of the vector.
*/
void Vector::push_back(const int& val)
{
	if (this->_size >= this->_capacity)
	{
		this->_capacity += this->_resizeFactor;
	}
	this->_size++;
	int* newArr = new int[this->_size];
	for (int i = 0;i < this->_size - 1;i++)
	{
		newArr[i] = this->_elements[i];
	}
	newArr[this->_size-1] = val;
	delete[] this->_elements;
	this->_elements = newArr;
}
/*
function to get the last vector value and delete it
*/
int Vector::pop_back()
{
	int val = this->_elements[this->_size-1];
	int* newArray = new int[this->_size-1];
	for (int i = 0;i < this->_size - 1;i++)
	{
		newArray[i] = this->_elements[i];
	}
	this->_size -= 1;
	delete[] this->_elements;
	this->_elements = newArray;
	return val;
}
/*
reserve function to change the capacity appropriately
*/
void Vector::reserve(int n)
{
	while (this->_capacity < n)
	{
		this->_capacity += this->_resizeFactor;
	}
}
void Vector::resize(int n)
{
	//---------
}
/*function to assign a certain value to all the elements*/
void Vector::assign(int val)
{
	for (int i = 0;i < this->_size;i++)
	{
		this->_elements[i] = val;
	}
}
/*function to resize the vector appropriately*/
void Vector::resize(int n, const int& val)
{
	int i = 0;
	int* newArr = new int[n];
	for (i = 0; i < n; i++)
	{
		if (i < this->_size) 
		{
			newArr[i] = this->_elements[i];
		}
		else
		{
			newArr[i] = val;
		}
	}
	this->_size = n;
	delete[] this->_elements;
	this->_elements = newArr;
}
/*the copy function but with the = operator.
the function transfers the data from one vector to another through only the = operator*/
Vector& Vector::operator=(const Vector& other)
{
	delete[] this->_elements;
	this->_elements = other._elements;
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	return *this;
}
/*the copy constructor.
it copies all the data from one constructor to another*/
Vector::Vector(const Vector& other)
{
	this->_elements = other._elements;
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
}