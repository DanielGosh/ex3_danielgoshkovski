#pragma once
class Vector
{
private:
	int _size;
	int _capacity;
	int _resizeFactor;
	//int* _elements;
public:
	//i am aware that elements is a private value but im using it in public to prove the functions work
	int* _elements;
	Vector(int n);
	~Vector();
	int size();
	int capicity();
	int resizeFactor();
	bool empty();
	void push_back(const int& val);
	int pop_back();
	void reserve(int n);
	void resize(int n);
	void assign(int val);
	void resize(int n, const int& val);
	//you only need the copy and the =, because the deconstructor is already made:
	Vector& operator=(const Vector& other);
	Vector(const Vector& other);
};

