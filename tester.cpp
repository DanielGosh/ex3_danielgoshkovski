#include <iostream>
#include "Vector.h"
using namespace std;
int main()
{
	Vector* a = new Vector(3);
	cout << "size, capacity: " << a->size() << "," << a->capicity();
	a->assign(9);
	for (int i = 0;i < a->size();i++)
	{
		cout << "\na val: " << a->_elements[i];
	}
	a->push_back(5);
	cout << "\nlast val: " << a->_elements[a->size() - 1];
	int pop = a->pop_back();
	cout << "\nlast val: " << a->_elements[a->size() - 1];
	a->reserve(32);
	cout << "\nnew reserve: " << a->capicity();
	a->resize(5, 6);
	for (int i = 0; i < a->size(); i++)
	{
		cout << "\na val: " << a->_elements[i];
	}
	Vector* b = new Vector(1);
	cout << "\nnew vector size(even though set as 1): " << b->size();
	b->_elements[0] = 1;
	b->_elements[1] = 3;
	cout << "\nb val pre equal operator:";
	for (int i = 0; i < b->size(); i++)
	{
		cout << "\nb val: " << b->_elements[i];
	}
	b = a;
	cout << "\nb val post equal operator:";
	for (int i = 0; i < b->size(); i++)
	{
		cout << "\nb val: " << b->_elements[i];
	}
	return 0;
}
